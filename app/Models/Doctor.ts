import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import Employee from './Employee'

export default class Doctor extends BaseModel {
  // !NOTE : isi field didalam model harus sama dengan yang ada di table database
  // !NOTE : penamaan tipe data & field harus sama, kecuali yang under_scores dijadikan camelCase

  @column({ isPrimary: true })
  public id: string

  @column()
  public employeeId: string

  @belongsTo(() => Employee)
  public karyawan: BelongsTo<typeof Employee>

  @column()
  public licenseNumber: string

  @column()
  public specialization: string

  @column()
  public fee: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
