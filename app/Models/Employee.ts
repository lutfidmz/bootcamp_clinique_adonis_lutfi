import { DateTime } from 'luxon'
import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import Doctor from './Doctor'

export default class Employee extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public name: string

  @column()
  public username: string

  @column()
  public password: string

  @column()
  public nik: string

  @column()
  public role: string

  @column.date()
  public joinDate: DateTime

  @column()
  public phoneNumber: string

  @column()
  public address: string

  @column()
  public email: string

  @column()
  public specialization: string

  @column()
  public gender: string

  @hasOne(() => Doctor)
  public doctor: HasOne<typeof Doctor>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
