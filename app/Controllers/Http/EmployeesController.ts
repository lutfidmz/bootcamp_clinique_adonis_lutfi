import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import Database from '@ioc:Adonis/Lucid/Database'
import Employee from 'App/Models/Employee'
import { v4 as uuidv4 } from 'uuid'

export default class EmployeesController {
  public async index({ response, request }: HttpContextContract) {
    const { keyword = "" } = request.qs()

    // const data = await Database
    //   .query()
    //   .from('employees')
    //   .select("id", "name", "username", "role", "join_date", "email")
    //   .whereILike('name', `%${keyword}%`)

    const dataModel = await Employee
      .query()
      .select("id", "name", "username", "role", "join_date", "email")
      .preload('doctor')
      .whereILike('name', `%${keyword}%`)

    response.ok({
      message: "Berhasil mengambil seluruh data Employee",
      data: dataModel
    })
  }

  public async store({ request, response }: HttpContextContract) {
    const objEmployee = request.body()
    objEmployee['id'] = uuidv4()

    // const newEmployee = await Database
    //   .table('clinique.employees')
    //   .returning('*')
    //   .insert(objEmployee)

    const newEmployeeModel = await Employee.create(objEmployee)

    response.created({
      message: "Berhasil menyimpan data Employee Baru",
      data: newEmployeeModel
    })
  }

  public async show({ params, response }: HttpContextContract) {
    const { id } = params

    // const data = await Database
    //   .from('clinique.employees')
    //   .where('id', id)

    const data = await Employee.findOrFail(id) // where id = $id

    response.ok({
      message: "Berhasil mengambil data Employee",
      data
    })
  }

  public async update({ params, request, response }: HttpContextContract) {
    const { id } = params
    const employeeData = request.body()

    //coding update data berdasarkan ID
    // const data = await Database
    //   .from('clinique.employees')
    //   .where('id', id)
    //   .update(employeeData, '*')

    const data = await Employee.findOrFail(id)
    await data.merge(employeeData).save()

    response.ok({
      message: "Berhasil mengubah data Employee",
      data
    })
  }

  public async destroy({ params, response }: HttpContextContract) {
    const { id } = params
    // const data = await Database
    //   .from('clinique.employees')
    //   .where('id', id)
    //   .delete()

    const data = await Employee.findOrFail(id)
    await data.delete()

    response.ok({
      message: `Berhasil menghapus data ${data.name} Employee`,
      // data: {
      //   jumlah_data_dihapus: data
      // }
    })
  }
}