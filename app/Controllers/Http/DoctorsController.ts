import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import Doctor from 'App/Models/Doctor'
import { v4 as uuidv4 } from 'uuid'

export default class DoctorsController {
  public async index({ response }: HttpContextContract) {
    // const allDoctors = await Database
    //   .from('doctors')
    //   .join('employees', 'employees.id', '=', 'doctors.employee_id')
    //   .select(
    //     'doctors.id', 'doctors.employee_id'
    //     , 'employees.name as employee_name'
    //     , 'employees.email as employee_email'
    //     , 'employees.phone_number as employee_phone'
    //     , 'doctors.specialization'
    //     , 'doctors.license_number'
    //     , 'doctors.fee'
    //   )

    const allDoctorsFromModel = await Doctor
      .query()
      .select('id', 'employee_id', 'specialization', 'license_number', 'fee')
      .preload('karyawan', employee => employee.select('id', 'name'))

    response.ok({
      message: "Berhasil mengambil data seluruh dokter",
      data: allDoctorsFromModel
    })
  }

  public async store({ request, response }: HttpContextContract) {
    const doctorObj = request.body()

    // todo(optional): handling response untuk case duplicate foreign key
    const newRecord = await Database
      .table("doctors")
      .returning('*')
      .insert({ id: uuidv4(), ...doctorObj })

    response.ok({
      message: "berhasil menyimpan data semua dokter",
      data: newRecord
    })
  }

  public async show({ params, response }: HttpContextContract) {
    const { id } = params
    try {
      // const doctorData = await Database
      //   .from('doctors')
      //   .join('employees', 'employees.id', '=', 'doctors.employee_id')
      //   .select(
      //     'doctors.id', 'doctors.employee_id'
      //     , 'employees.name as employee_name'
      //     , 'employees.email as employee_email'
      //     , 'employees.phone_number as employee_phone'
      //     , 'doctors.specialization'
      //     , 'doctors.license_number'
      //     , 'doctors.fee'
      //   )
      //   .where('doctors.id', id)
      //   .firstOrFail()


      const data = await Doctor.query()
        .preload('karyawan')
        .where('id', id)
        .firstOrFail()

      response.ok({
        message: "Berhasil mengambil data dokter",
        data
      })
    } catch (error) {
      const errMsg = "DCTL 66: " + error.message || error
      console.log(errMsg);
      response.badRequest({ message: "Gagal mengambil data dokter", error: errMsg })
    }

  }

  public async update({ params, request, response }: HttpContextContract) {
    const { id } = params
    const reqBody = request.body()

    const updatedData = await Database
      .from('doctors')
      .where('id', id)
      .update(reqBody, '*')

    if (updatedData.length <= 0) {
      response.notFound({
        message: "Gagal update: data dokter tidak ditemukan",
      });
    } else {
      response.ok({
        message: "Berhasil mengubah data dokter",
        data: updatedData,
      });
    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    const { id } = params

    const deletedRowsCount = await Database
      .from("doctors")
      .where("id", id)
      .delete();

    response.ok({
      message: "Berhasil menghapus data",
      data: {
        deletedRowsCount
      }
    })
  }
}
