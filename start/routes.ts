/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

// !NOTE: ini adalah ROUTING
import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})


/**
 Route.post('/employees', async ({ request, response }) => {
   const objEmployee = request.body()
 
   response.created({
     message: "Berhasil menyimpan data Employee",
     data: Object.values(objEmployee)
   })
 })
 * 
 */

// Route.get('/employees', 'EmployeesController.index')
// Route.get('/employees/:id', 'EmployeesController.show')
// Route.post('/employees', 'EmployeesController.store')
// Route.put('/employees/:id', 'EmployeesController.update')
// Route.delete('/employees/:id', 'EmployeesController.destroy')

Route.resource('/employees', 'EmployeesController').apiOnly()
Route.resource('/doctors', 'DoctorsController').apiOnly()